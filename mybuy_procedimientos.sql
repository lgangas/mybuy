use myBuy;

delimiter $$
create PROCEDURE sp_login_usuario (in_numero_telefonico VARCHAR(20), 
				in_contraseña VARCHAR(20))
BEGIN
         select a.*,avg(b.puntaje) promedio_puntaje,count(c.codigo_comentario) cantidad_comentarios from usuario a 
		 left join puntaje b 
		 on a.codigo_usuario=b.codigo_usuario_puntuado 
	     left join comentario c
		 on a.codigo_usuario=c.codigo_usuario_receptor
            WHERE STRCMP(numero_telefonico,in_numero_telefonico)=0 
				AND STRCMP(contrasenia,in_contraseña)=0;
                
END $$

call sp_login_usuario ('972287773','123456')




delimiter $$
create procedure sp_registrar_usuario ( in v_numero_tel char(9),
										in v_contrasenia varchar(20),
										in v_nickname varchar(20),
										in v_categoria varchar(30),
										in v_numero_cuenta_bancaria varchar(20),
										in v_descripcion varchar(200))
begin

 DECLARE EXIT HANDLER FOR SQLEXCEPTION 
 SELECT 'Ha ocurrido un error al registrar el usuario';

insert into usuario values 
(default,
v_numero_tel,
v_contrasenia ,
v_nickname ,
v_categoria ,
v_numero_cuenta_bancaria ,
v_descripcion );


end $$

call sp_registrar_usuario ('968357241','123','lmarano','perfumes','8888 6666 4444 2222','Los mejores perfumes.')



delimiter $$
create procedure sp_buscarxtelefono 
(in v_telefono char(9))
begin

select * from usuario
where numero_telefonico like concat('%',v_telefono,'%');

end $$
call sp_buscarxtelefono('9');

delimiter $$
create procedure sp_datosperfil (in v_codigo int)

begin
select 
a.*,
avg(b.puntaje) promedio_puntaje,
count(distinct c.codigo_comentario) cantidad_comentarios from usuario a 
left join puntaje b 
on a.codigo_usuario=b.codigo_usuario_puntuado 
left join comentario c
on a.codigo_usuario=c.codigo_usuario_receptor
where a.codigo_usuario=v_codigo;

end $$

select* from usuario where codigo_usuario = 2

call sp_datosperfil (5) ;

delimiter $$
create procedure sp_registrarcomentario (in v_codigo_emisor int,in v_codigo_receptor int, in v_comentario varchar(200))
begin

insert into comentario values (default,v_codigo_emisor,v_codigo_receptor,v_comentario);

end $$

call sp_registrarcomentario(2,1,'too bien 100% real');

delimiter $$
create procedure sp_registrarpuntaje (in v_codigo_emisor int,in v_codigo_receptor int, in v_puntaje int)
begin

insert into puntaje values (default,v_codigo_receptor,v_codigo_emisor,v_puntaje);

end $$

call sp_registrarpuntaje(1,3,6)

select * from puntaje;

delimiter $$
create procedure sp_listacomentario (in v_codigo int)
begin

select a2.nickname,c.comentario from usuario a 
join comentario c 
on a.codigo_usuario=c.codigo_usuario_receptor
join usuario a2 
on a2.codigo_usuario = c.codigo_usuario_emisor
where a.codigo_usuario=v_codigo;

end $$

call sp_listacomentario(3);

select * from usuario;

-- crear chat
delimiter $$
create procedure sp_crearChat (in v_codigo_usuario_1 int,in v_codigo_usuario_2 int)
begin

insert into chat values (default,v_codigo_usuario_1,v_codigo_usuario_2);

end $$

select * from chat;
select *from detalle_chat;

call sp_crearChat(1,2);
-- crear detalle_chat
delimiter $$
create procedure sp_crearDetalleChat (in v_codigo_chat int,in v_codigo_usuario_1 int,in v_codigo_usuario_2 int,in v_codigo_compra int,in v_mensaje varchar(200),in v_tipo_mensaje smallint)
begin

insert into detalle_chat values (default,v_codigo_chat,v_codigo_usuario_1,v_codigo_usuario_2,v_codigo_compra,v_mensaje,v_tipo_mensaje);

end $$

call sp_crearDetalleChat(1,2,1,null,'hola que tal?',0);

-- listar detalles_chat
delimiter $$
create procedure sp_listarDetalleChat (in v_codigo_usuario_1 int,in v_codigo_usuario_2 int)
begin

select
dc.codigo_detalle_chat,
u1.codigo_usuario codigo_usuario1,
u2.codigo_usuario codigo_usuario2,
u1.nickname usuario1,
u2.nickname usuario2,
dc.mensaje,
dc.tipo_mensaje,
co.codigo_compra,
co.monto,
co.aceptacion_venta_comprador ventaComprador,
co.aceptacion_venta_vendedor ventaVendedor,
co.aceptacion_compra_vendedor compraVendedor,
co.aceptacion_compra_comprador compraComprador, 
co.estado
from detalle_chat dc
inner join usuario u1 on u1.codigo_usuario = dc.codigo_usuario_emisor
inner join usuario u2 on u2.codigo_usuario = dc.codigo_usuario_receptor
left join compra co on co.codigo_compra = dc.codigo_compra
where u1.codigo_usuario in (v_codigo_usuario_1,v_codigo_usuario_2)
and u2.codigo_usuario in (v_codigo_usuario_1,v_codigo_usuario_2)
order by codigo_detalle_chat desc;

end $$

call sp_listarDetalleChat(1,2);
call sp_listarDetalleChat(2,1);
select * from usuario;

-- registrar compra
delimiter $$
create procedure sp_crearCompra (in v_codigo_usuario_1 int,in v_codigo_usuario_2 int,in v_monto double)
begin

insert into compra values (default,v_codigo_usuario_1,v_codigo_usuario_2,v_monto,1,0,0,0,0);

end $$
delete from compra where codigo_compra in (3)
call sp_crearCompra(1,2,1200);

select * from detalle_chat;
select * from compra;

-- listar chat por persona
delimiter $$
create procedure sp_listarChat (in v_codigo_usuario int)
begin

select 
c.codigo_chat,
codigo_usuario_1,
u2.nickname,
codigo_usuario_2,
dc.mensaje,
dc.tipo_mensaje
from chat c
inner join usuario u1 on u1.codigo_usuario = c.codigo_usuario_1
inner join usuario u2 on u1.codigo_usuario = c.codigo_usuario_2
inner join detalle_chat dc on dc.codigo_chat = c.codigo_chat
where codigo_usuario_1 = v_codigo_usuario
order by dc.codigo_detalle_chat desc
limit 1;

end $$

select * from chat;
select * from detalle_chat;
delete from detalle_chat where codigo_detalle_chat > 1;
select * from compra;
-- listar codigo_chat
delimiter $$
create procedure sp_listarCodigoChat (in v_codigo_usuario1 int,in v_codigo_usuario2 int)
begin

select 
codigo_chat
from chat
where codigo_usuario_1 in (v_codigo_usuario1,v_codigo_usuario2)
and codigo_usuario_2 in (v_codigo_usuario1,v_codigo_usuario2);

end $$

call sp_listarCodigoChat(2,1);