package com.mybuy.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mybuy.beans.Mensaje;
import com.mybuy.beans.Usuario;
import com.mybuy.dao.MensajeDao;
import com.mybuy.dao.UsuarioDao;
import java.sql.SQLException;

@Path("/MyBuyRest")
public class UsuarioRest {

    @POST
    @Path("/ingresarUsuario")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Usuario ingresarUsuario(Usuario usuario) {
        return new UsuarioDao().ingresarUsuario(usuario.getNumero_telefono(), usuario.getContrasenia());
    }

    @POST
    @Path("/registrarUsuario")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int registrarUsuario(Usuario usuario) {
        return new UsuarioDao().registrarUsuario(usuario);
    }

    @POST
    @Path("/buscarTelefono")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Usuario> buscarTelefono(Usuario usuario) {
        return new UsuarioDao().buscarTelefono(usuario.getNumero_telefono());
    }

    @POST
    @Path("/datosPerfil")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Usuario datosPerfil(Usuario usuario) {
        return new UsuarioDao().datosPerfil(usuario.getCodigo_usuario());
    }

    @POST
    @Path("/listarComentarios")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Usuario> listarComentarios(Usuario usuario) {
        return new UsuarioDao().listarComentarios(usuario.getCodigo_usuario());
    }

    @POST
    @Path("/registrarComentario")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int registrarComentario(Usuario usuario) {
        return new UsuarioDao().registrarComentario(usuario.getCodigo_usuario_2(), usuario.getCodigo_usuario(), usuario.getComentario());
    }

    @POST
    @Path("/registrarPuntaje")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int registrarPuntaje(Usuario usuario) {
        return new UsuarioDao().registrarPuntaje(usuario.getCodigo_usuario_2(), usuario.getCodigo_usuario(), usuario.getPuntaje());
    }

    @POST
    @Path("/listarMensajes")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Mensaje> listarMensaje(Usuario usuario) {
        return new MensajeDao().listarMensaje(usuario);
    }
		
    @POST
    @Path("/registrarMensaje")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int registrarMensaje(Mensaje mensaje) throws SQLException {
        return new MensajeDao().registrarMensaje(mensaje);
    }
    @POST
    @Path("/registrarCompra")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int registrarCompra(Mensaje mensaje) throws SQLException {
        return new MensajeDao().registrarCompra(mensaje);
    }
}
