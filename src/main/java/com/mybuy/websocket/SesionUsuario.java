/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mybuy.websocket;

import com.mybuy.beans.Usuario;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author sistem20user
 */
@ServerEndpoint(value = "/UsuarioSesion/{idUsuario}",
	configurator = GetHttpSessionConfigurator.class)
public class SesionUsuario {

	private static List<Usuario> sessions = new CopyOnWriteArrayList<Usuario>();
	private static Set<Integer> nombres = Collections.synchronizedSet(new HashSet<Integer>());
	private static Set<Session> wsSessions = Collections.synchronizedSet(new HashSet<Session>());
	private HttpSession httpSession;
	int userLogin = 0;
//    public void open(Session session, EndpointConfig config) {

	@OnOpen
	public void open(Session session, EndpointConfig config, @PathParam("idUsuario") String msg) {
		httpSession = (HttpSession) config.getUserProperties()
			.get(HttpSession.class.getName());
		userLogin = Integer.parseInt(msg);
		if (userLogin != 0) {
			if (!nombres.contains(userLogin)) {
				Usuario obj = new Usuario(session, userLogin);
				nombres.add(userLogin);
				sessions.add(obj);
				wsSessions.add(session);
			}
		}
	}

	@OnMessage
	public void println(String msg) throws IOException {
		if (msg.equals("listarMensajes")) {
			for (Usuario u : sessions) {
				System.out.println("" + u.getCodigo_usuario());
				u.getSesion().getBasicRemote().sendText(msg);
			}
		}
	}

	@OnClose
	public void close(Session session) throws IOException {
		for (Usuario u : sessions) {
			if (u.getSesion().getId().equals(session.getId())) {
				sessions.remove(u);
				nombres.remove(u.getCodigo_usuario());
				wsSessions.remove(session);
			}
		}
		println("");
	}
}
