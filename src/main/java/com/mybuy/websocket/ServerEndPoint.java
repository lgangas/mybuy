package com.mybuy.websocket;

import java.util.ArrayList;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/myEndPoint")
public class ServerEndPoint {

	ArrayList<String> listaSesiones = new ArrayList<String>();

	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Open");
		String id = session.getId();
		listaSesiones.add(id);
		System.out.println(id);
	}

	@OnMessage
	public String onMessage(String message) {
		System.out.println(message);
		return "Este es el mensaje" + message;
	}

	@OnError
	public void onError(Throwable t) {
		System.out.println("Error" + t.getMessage());
	}

	@OnClose
	public void onClose() {
		System.out.println("Close");
	}

}
