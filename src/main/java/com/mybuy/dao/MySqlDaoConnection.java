package com.mybuy.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySqlDaoConnection {

	public Connection getConnection() {

		Connection cn = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			cn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/myBuy?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "mysql");

			System.out.println("Exito al conectarse.");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cn;
	}

	public static void main(String[] args) {
		new MySqlDaoConnection().getConnection();
	}
}
