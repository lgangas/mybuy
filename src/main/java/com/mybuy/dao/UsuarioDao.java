package com.mybuy.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mybuy.beans.Usuario;

public class UsuarioDao {
	
	public Usuario ingresarUsuario(String telefono, String contrasenia) {
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_login_usuario(?,?)}");
			
			cs.setString(1, telefono);
			cs.setString(2, contrasenia);
			
			rs = cs.executeQuery();
			
			if(rs.next()) {
				usuario = new Usuario();
				int q = 1;
				usuario.setCodigo_usuario(rs.getInt(q++));
				usuario.setNumero_telefono(rs.getString(q++));
				usuario.setContrasenia(rs.getString(q++));
				usuario.setNickname(rs.getString(q++));
				usuario.setCategoria(rs.getString(q++));
				usuario.setNumero_cuenta_bancaria(rs.getString(q++));
				usuario.setDescripcion(rs.getString(q++));				
				usuario.setPromedio_puntaje(rs.getDouble(q++));				
				usuario.setCantidad_mensajes(rs.getInt(q++));				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return usuario;
	}
	
	public Usuario datosPerfil(int codigo) {
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_datosperfil(?)}");
			
			cs.setInt(1, codigo);
			
			rs = cs.executeQuery();
			
			if(rs.next()) {
				usuario = new Usuario();
				int q = 1;
				usuario.setCodigo_usuario(rs.getInt(q++));
				usuario.setNumero_telefono(rs.getString(q++));
				usuario.setContrasenia(rs.getString(q++));
				usuario.setNickname(rs.getString(q++));
				usuario.setCategoria(rs.getString(q++));
				usuario.setNumero_cuenta_bancaria(rs.getString(q++));
				usuario.setDescripcion(rs.getString(q++));				
				usuario.setPromedio_puntaje(rs.getDouble(q++));				
				usuario.setCantidad_mensajes(rs.getInt(q++));				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return usuario;
	}
	
	public int registrarUsuario(Usuario usuario) {
		Connection cn = null;
		CallableStatement cs = null;
		int rs = 0;
		
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_registrar_usuario(?,?,?,?,?,?)}");
			
			int q = 1;
			cs.setString(q++, usuario.getNumero_telefono());
			cs.setString(q++, usuario.getContrasenia());
			cs.setString(q++, usuario.getNickname());
			cs.setString(q++, usuario.getCategoria());
			cs.setString(q++, usuario.getNumero_cuenta_bancaria());
			cs.setString(q++, usuario.getDescripcion());
			
			rs = cs.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return rs;
	}
	
	public List<Usuario> buscarTelefono(String telefono) {
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_buscarxtelefono(?)}");
			
			cs.setString(1, telefono);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Usuario usuario = new Usuario();
				int q = 1;
				usuario.setCodigo_usuario(rs.getInt(q++));
				usuario.setNumero_telefono(rs.getString(q++));
				usuario.setContrasenia(rs.getString(q++));
				usuario.setNickname(rs.getString(q++));
				usuario.setCategoria(rs.getString(q++));
				usuario.setNumero_cuenta_bancaria(rs.getString(q++));
				usuario.setDescripcion(rs.getString(q++));
				
				listaUsuario.add(usuario);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return listaUsuario;
	}
	
	public int registrarComentario(int codigo_usuario_2,int codigo_usuario, String comentario) {
		Connection cn = null;
		CallableStatement cs = null;
		int rs = 0;	
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_registrarcomentario(?,?,?)}");
			
			cs.setInt(1, codigo_usuario_2);
			cs.setInt(2, codigo_usuario);
			cs.setString(3, comentario);
			
			rs = cs.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return rs;
	}
	
	public int registrarPuntaje(int codigo_usuario_2,int codigo_usuario, int puntaje) {
		Connection cn = null;
		CallableStatement cs = null;
		int rs = 0;	
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_registrarpuntaje(?,?,?)}");
			
			cs.setInt(1, codigo_usuario_2);
			cs.setInt(2, codigo_usuario);
			cs.setInt(3, puntaje);
			
			rs = cs.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return rs;
	}
	
	public List<Usuario> listarComentarios(int codigo) {
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		
		
		try{
			cn = new MySqlDaoConnection().getConnection();
			cs = cn.prepareCall("{call sp_listacomentario(?)}");
			
			cs.setInt(1, codigo);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Usuario usuario = new Usuario();
				int q = 1;
				usuario.setNickname(rs.getString(q++));
				usuario.setComentario(rs.getString(q++));
				
				listaUsuario.add(usuario);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return listaUsuario;
	}
	
	public static void main(String[] args) {
//		Usuario usu = new UsuarioDao().datosPerfil(1);
//		if(usu.getNumero_telefono() != null) {
//			System.out.println("Hola " + usu.getNickname());
//		} else {
//			System.out.println("No se ha encontrado usuario.");
//		}
		
//		List<Usuario> usu = new UsuarioDao().listarComentarios(1);
//		if(usu != null) {
//			System.out.println(usu.get(0).getComentario());
//		} else {
//			System.out.println("No se ha encontrado usuario.");
//		}
		
		int respuesta = new UsuarioDao().registrarPuntaje(1,3,9);
		if(respuesta == 1) {
			System.out.println("Registro exitoso.");
		} else {
			System.out.println("No se ha registrado el puntaje.");
		}
	}
	
	
	
}
