package com.mybuy.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mybuy.beans.Mensaje;
import com.mybuy.beans.Usuario;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;

public class MensajeDao {

    public List<Mensaje> listarMensaje(Usuario usuario) {
        Connection cn = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        List<Mensaje> listaMensaje = new ArrayList<Mensaje>();

        try {
            cn = new MySqlDaoConnection().getConnection();
            cs = cn.prepareCall("{call sp_listarDetalleChat(?,?)}");

            cs.setInt(1, usuario.getCodigo_usuario());
            cs.setInt(2, usuario.getCodigo_usuario_2());

            rs = cs.executeQuery();

            while (rs.next()) {
                Mensaje mensaje = new Mensaje();

                mensaje.setCodigo_detalle_chat(rs.getString("codigo_detalle_chat"));
                mensaje.setCodigo_usuario_1(rs.getString("codigo_usuario1"));
                mensaje.setCodigo_usuario_2(rs.getString("codigo_usuario2"));
                mensaje.setUsuario1(rs.getString("usuario1"));
                mensaje.setUsuario2(rs.getString("usuario2"));
                mensaje.setMensaje(rs.getString("mensaje"));
                mensaje.setTipo_mensaje(rs.getString("tipo_mensaje"));
                mensaje.setCodigo_compra(rs.getString("codigo_compra"));
                mensaje.setMonto(rs.getDouble("monto"));
                mensaje.setVentaComprador(rs.getInt("ventaComprador"));
                mensaje.setVentaVendedor(rs.getInt("ventaVendedor"));
                mensaje.setCompraVendedor(rs.getInt("compraVendedor"));
                mensaje.setCompraComprador(rs.getInt("compraComprador"));
                mensaje.setEstado(rs.getInt("estado"));

                listaMensaje.add(mensaje);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return listaMensaje;

    }

    public int registrarMensaje(Mensaje mensaje) throws SQLException {
        Connection cn = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        String respuesta = "";
        int respuestaInt = 0;

        try {
            cn = new MySqlDaoConnection().getConnection();
            cn.setAutoCommit(false);
            cs = cn.prepareCall("{call sp_listarCodigoChat(?,?)}");

            cs.setString(1, mensaje.getCodigo_usuario_1());
            cs.setString(2, mensaje.getCodigo_usuario_2());

            rs = cs.executeQuery();
            int codigo = 0;
            if (rs.next()) {
                codigo = rs.getInt("codigo_chat");
            }

            if (codigo == 0) {
                PreparedStatement pst = cn.prepareStatement("insert into chat values (default,?,?)", Statement.RETURN_GENERATED_KEYS);
                pst.setString(1, mensaje.getCodigo_usuario_1());
                pst.setString(2, mensaje.getCodigo_usuario_2());

                int rsCrearChat = pst.executeUpdate();
                if (rsCrearChat > 0) {
                    ResultSet rsKey = pst.getGeneratedKeys();
                    rsKey.next();
                    codigo = rsKey.getInt(1);
                } else {
                    throw new Exception("error");
                }
            }

            cs = cn.prepareCall("{call sp_crearDetalleChat(?,?,?,?,?,?)}");

            cs.setInt(1, codigo);
            cs.setString(2, mensaje.getCodigo_usuario_1());
            cs.setString(3, mensaje.getCodigo_usuario_2());
            if (mensaje.getCodigo_compra() == null) {
                cs.setNull(4, Types.INTEGER);
            } else {
                cs.setString(4, mensaje.getCodigo_compra());
            }
            cs.setString(5, mensaje.getMensaje());
            cs.setString(6, mensaje.getTipo_mensaje());

            int rsCrearDetalleChat = cs.executeUpdate();

            if (rsCrearDetalleChat > 0) {
                respuesta = "success";
                respuestaInt = 1;
                cn.commit();
            } else {
                respuesta = "error";
                cn.rollback();
            }

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
            cn.rollback();
            // TODO: handle exception
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return respuestaInt;
    }

    public int registrarCompra(Mensaje mensaje) throws SQLException {
        Connection cn = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        String respuesta = "";
        int respuestaInt = 0;

        try {
            cn = new MySqlDaoConnection().getConnection();
            cn.setAutoCommit(false);

            PreparedStatement pstCompra
                    = cn.prepareStatement("insert into compra values (default,?,?,?,1,0,0,0,0)", Statement.RETURN_GENERATED_KEYS);
            pstCompra.setString(1, mensaje.getCodigo_usuario_1());
            pstCompra.setString(2, mensaje.getCodigo_usuario_2());
            pstCompra.setDouble(3, mensaje.getMonto());

            int rsCompra = pstCompra.executeUpdate();
            if (rsCompra > 0) {
                ResultSet rsetCompra = pstCompra.getGeneratedKeys();
                rsetCompra.next();
                int keyCompra = rsetCompra.getInt(1);

                cs = cn.prepareCall("{call sp_listarCodigoChat(?,?)}");

                cs.setString(1, mensaje.getCodigo_usuario_1());
                cs.setString(2, mensaje.getCodigo_usuario_2());

                rs = cs.executeQuery();
                int codigo = 0;
                if (rs.next()) {
                    codigo = rs.getInt("codigo_chat");
                }

                if (codigo == 0) {
                    PreparedStatement pst = cn.prepareStatement("insert into chat values (default,?,?)", Statement.RETURN_GENERATED_KEYS);
                    pst.setString(1, mensaje.getCodigo_usuario_1());
                    pst.setString(2, mensaje.getCodigo_usuario_2());

                    int rsCrearChat = pst.executeUpdate();
                    if (rsCrearChat > 0) {
                        ResultSet rsKey = pst.getGeneratedKeys();
                        rsKey.next();
                        codigo = rsKey.getInt(1);
                    } else {
                        throw new Exception("error");
                    }
                }

                cs = cn.prepareCall("{call sp_crearDetalleChat(?,?,?,?,?,?)}");

                cs.setInt(1, codigo);
                cs.setString(2, mensaje.getCodigo_usuario_1());
                cs.setString(3, mensaje.getCodigo_usuario_2());
                cs.setInt(4, keyCompra);
                cs.setString(5, mensaje.getMensaje());
                cs.setString(6, mensaje.getTipo_mensaje());

                int rsCrearDetalleChat = cs.executeUpdate();

                if (rsCrearDetalleChat > 0) {
                    respuesta = "success";
                    respuestaInt = 1;
                    cn.commit();
                } else {
                    respuesta = "error";
                    cn.rollback();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
            cn.rollback();
            // TODO: handle exception
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return respuestaInt;
    }

}
