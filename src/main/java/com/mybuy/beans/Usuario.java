package com.mybuy.beans;

import com.mybuy.websocket.SesionUsuario;
import javax.websocket.Session;


public class Usuario {
	private int codigo_usuario;
	private String numero_telefono;
	private String contrasenia;
	private String nickname;
	private String categoria;
	private String numero_cuenta_bancaria;
	private String descripcion;
	
	private int codigo_usuario_2;
	private String nickname_2;
		
	private String comentario;
	private int puntaje;
	
	private double promedio_puntaje;
	private int cantidad_mensajes;
	
	private Session sesion;

	public Session getSesion() {
		return sesion;
	}

	public void setSesion(Session sesion) {
		this.sesion = sesion;
	}
	
	
	
	public Usuario(Session session, int codigo_usuario){
		this.sesion = session;
		this.codigo_usuario = codigo_usuario;
	}
	
	public Usuario(String numero_telefono, String contrasenia, String nickname, String categoria,
			String numero_cuenta_bancaria, String descripcion) {
		super();
		this.numero_telefono = numero_telefono;
		this.contrasenia = contrasenia;
		this.nickname = nickname;
		this.categoria = categoria;
		this.numero_cuenta_bancaria = numero_cuenta_bancaria;
		this.descripcion = descripcion;
	}
	
	public Usuario() {}
	
	public int getPuntaje() {
		return puntaje;
	}
	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}
	public String getNickname_2() {
		return nickname_2;
	}
	public void setNickname_2(String nickname_2) {
		this.nickname_2 = nickname_2;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public int getCodigo_usuario_2() {
		return codigo_usuario_2;
	}
	public void setCodigo_usuario_2(int codigo_usuario_2) {
		this.codigo_usuario_2 = codigo_usuario_2;
	}
	public double getPromedio_puntaje() {
		return promedio_puntaje;
	}
	public void setPromedio_puntaje(double promedio_puntaje) {
		this.promedio_puntaje = promedio_puntaje;
	}
	public int getCantidad_mensajes() {
		return cantidad_mensajes;
	}
	public void setCantidad_mensajes(int cantidad_mensajes) {
		this.cantidad_mensajes = cantidad_mensajes;
	}
	public int getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(int codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	public String getNumero_telefono() {
		return numero_telefono;
	}
	public void setNumero_telefono(String numero_telefono) {
		this.numero_telefono = numero_telefono;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getNumero_cuenta_bancaria() {
		return numero_cuenta_bancaria;
	}
	public void setNumero_cuenta_bancaria(String numero_cuenta_bancaria) {
		this.numero_cuenta_bancaria = numero_cuenta_bancaria;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
