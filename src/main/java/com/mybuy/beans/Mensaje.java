package com.mybuy.beans;

public class Mensaje {
	private String codigo_chat;
	private String codigo_detalle_chat;
	private String tipo_mensaje;
	private String mensaje;
	
	private String codigo_compra;
	private double monto;
	private int ventaComprador;
	private int ventaVendedor;
	private int compraComprador;
	private int compraVendedor;
	private int estado;
	
	private String codigo_usuario_1;
	private String codigo_usuario_2;
	private String usuario1;
	private String usuario2;
	
	
	
	
	public String getCodigo_detalle_chat() {
		return codigo_detalle_chat;
	}
	public void setCodigo_detalle_chat(String codigo_detalle_chat) {
		this.codigo_detalle_chat = codigo_detalle_chat;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public int getVentaComprador() {
		return ventaComprador;
	}
	public void setVentaComprador(int ventaComprador) {
		this.ventaComprador = ventaComprador;
	}
	public int getVentaVendedor() {
		return ventaVendedor;
	}
	public void setVentaVendedor(int ventaVendedor) {
		this.ventaVendedor = ventaVendedor;
	}
	public int getCompraComprador() {
		return compraComprador;
	}
	public void setCompraComprador(int compraComprador) {
		this.compraComprador = compraComprador;
	}
	public int getCompraVendedor() {
		return compraVendedor;
	}
	public void setCompraVendedor(int compraVendedor) {
		this.compraVendedor = compraVendedor;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getCodigo_usuario_1() {
		return codigo_usuario_1;
	}
	public void setCodigo_usuario_1(String codigo_usuario_1) {
		this.codigo_usuario_1 = codigo_usuario_1;
	}
	public String getCodigo_usuario_2() {
		return codigo_usuario_2;
	}
	public void setCodigo_usuario_2(String codigo_usuario_2) {
		this.codigo_usuario_2 = codigo_usuario_2;
	}
	public String getCodigo_chat() {
		return codigo_chat;
	}
	public void setCodigo_chat(String codigo_chat) {
		this.codigo_chat = codigo_chat;
	}
	public String getCodigo_compra() {
		return codigo_compra;
	}
	public void setCodigo_compra(String codigo_compra) {
		this.codigo_compra = codigo_compra;
	}
	public String getUsuario1() {
		return usuario1;
	}
	public void setUsuario1(String usuario1) {
		this.usuario1 = usuario1;
	}
	public String getUsuario2() {
		return usuario2;
	}
	public void setUsuario2(String usuario2) {
		this.usuario2 = usuario2;
	}
	public String getTipo_mensaje() {
		return tipo_mensaje;
	}
	public void setTipo_mensaje(String tipo_mensaje) {
		this.tipo_mensaje = tipo_mensaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
