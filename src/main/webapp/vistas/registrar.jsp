<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col s12">
				<h3>Registrar Usuario</h3>

				<div class="row">
					<div class="input-field col s12">
						<input id="txtTelefono" type="text" class="validate"> <label
							for="txtTelefono">Numero telefonico</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txtPassword" type="password" class="validate">
						<label for="txtPassword">Contraseņa</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txtNickname" type="text" class="validate"> <label
							for="txtNickname">Nickname</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txtCategoria" type="text" class="validate"> <label
							for="txtCategoria">Categoria</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txtNumeroCuenta" type="text" class="validate"> <label
							for="txtNumeroCuenta">Cuenta bancaria</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<textarea id="txtDescripcion" class="materialize-textarea"></textarea>
						<label for="txtDescripcion">Descripcion</label>
					</div>
				</div>
				
				<div class="row center">
					<button onclick="registrarUsuario()"
						class="waves-effect waves-light btn">Registrarse</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../js/registrar.js"></script>
</body>

</html>