<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insert title here</title>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">


        <!-- jQuery library -->
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Compiled and minified JavaScript -->
        <script
        src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body onload="listarMensajes()">

        <div style="height: 700px" id="divListaMensajes" class="col s12"></div>

        <div class="row" style="margin-bottom: 0px">
            <div class="col s10 input-field">
                <input id="txtMensaje" type="text" class="validate"> <label
                    for="txtMensaje">Escriba un mensaje</label>
            </div>
            <div class="col s1 center">
                <a onclick="enviarMensaje()"><i class="material-icons medium">forward</i></a>
            </div>
            <div class="col s1 center">
                <button data-target="modalcompra"
                        class="waves-effect waves-light btn-floating modal-trigger">
                    <i class="material-icons">import_export</i>
                </button>
            </div>
        </div>

        <!--modales-->
        <div id="modalcompra" class="modal">
            <div class="modal-content">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="txtMonto" class="validate" type="number">
                        <label for="txtMonto">Monto</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="registrarCompra()" class="waves-effect waves-red btn green">Aceptar</button>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
            </div>
        </div>
        <!--modales-->

        <script type="text/javascript" src="../js/chat.js"></script>
        <script src="../js/wsMensaje.js" type="text/javascript"></script>

    </body>
</html>