<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>



</head>

<body onload="cargarPerfil()">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<h3 id="lblNickaname"></h3>

				<div class="divider"></div>
				<div class="section">
					<span>Telefono</span>
					<p id="lblTelefono"></p>
				</div>
				<div class="divider"></div>
				<div class="section">
					<span>Categoria</span>
					<p id="lblCategoria"></p>
				</div>
				<div class="divider"></div>
				<div class="section">
					<span>Descripcion</span>
					<p id="lblDescripcion"></p>
				</div>
				<div class="divider"></div>
				<div class="section">
					<span>Puntaje
						<button data-target="modalPuntaje"
							class="waves-effect waves-light btn-floating modal-trigger">
							<i class="material-icons">add</i>
						</button>
					</span>
					<p id="lblPuntaje"></p>
				</div>
				<div class="divider"></div>
				<div class="section">
					<span id="lblCantidadComentario"></span>
					<button onclick="verComentarios()"
						class="waves-effect waves-light btn-floating orange">
						<i class="material-icons">remove_red_eye</i>
					</button>
					<button data-target="modalComentar"
						class="waves-effect waves-light btn-floating modal-trigger">
						<i class="material-icons">add</i>
					</button>
					<br> <br>
					<div class="row">
						<div class="container" id="divComentarios"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 	modales -->
	<div id="modalComentar" class="modal">
		<div class="modal-content">
			<div class="row">
				<div class="input-field col s12">
					<textarea id="txtComentario" class="materialize-textarea"></textarea>
					<label for="txtComentario">Comentario</label>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button onclick="comentar()" class="waves-effect waves-red btn green">Aceptar</button>
			<a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
		</div>
	</div>
	
	<div id="modalPuntaje" class="modal">
		<div class="modal-content">
			<div class="row">
				<div class="input-field col s12">
					<input id="txtPuntaje" class="validate" type="number">
					<label for="txtPuntaje">Puntaje</label>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button onclick="puntuar()" class="waves-effect waves-red btn green">Aceptar</button>
			<a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
		</div>
	</div>
	<!-- 	modales -->

	<script type="text/javascript" src="../js/verPerfil.js"></script>
</body>

</html>