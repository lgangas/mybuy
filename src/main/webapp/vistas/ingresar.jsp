<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>
	<div class="container">
		<div class="row center">
			<div class="col s12">
				<h1>My Buy</h1>

				<div class="row">
					<div class="input-field col s12">
						<input id="txtTelefono" type="text" class="validate"> <label
							for="txtTelefono">Numero telefonico</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txtPassword" type="password" class="validate">
						<label for="txtPassword">Contraseņa</label>
					</div>
				</div>
				<div class="row">
					<button onclick="ingresarUsuario()" class="waves-effect waves-light btn">Ingresar</button>
					<a href="registrar.jsp" class="waves-effect orange waves-light btn">Registrate</a>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../js/ingresar.js"></script>
</body>

</html>