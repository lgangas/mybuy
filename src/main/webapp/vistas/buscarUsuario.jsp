<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body onload="welcomeToast()">
	<nav class="nav-extended">
	<div class="nav-wrapper">
		<a href="#" class="brand-logo">My Buy</a> <a href="#"
			data-target="mobile-demo" class="sidenav-trigger"><i
			class="material-icons">menu</i></a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li><a href="ingresar.jsp">Cerrar sesion</a></li>
		</ul>
	</div>
	<div class="nav-content">
		<ul class="tabs tabs-transparent">
			<li class="tab col s4"><a href="#test1">Usuario</a></li>
			<li class="tab col s4"><a class="active" href="#test2">Buscar</a></li>
			<li class="tab col s4"><a href="#test4">Mensajes</a></li>
		</ul>
	</div>
	</nav>

	<ul class="sidenav" id="mobile-demo">
		<li><a href="ingresar.jsp">Cerrar sesion</a></li>
	</ul>

	<div class="container">
		<div class="row">
			<div class="col s12 input-field">
				<input id="txtBuscar" type="text" class="validate"> <label
					for="txtBuscar">Buscar usuario</label>
			</div>
			
		</div>
		<div class="row center">
				<button onclick="buscarUsuarios()" class="waves-effect waves-light btn">Buscar</button>
			</div>
		<div class="row" id="divListaUsuario">
		
		</div>
		<!-- 		<div id="test1" class="col s12">Test 1</div> -->
		<!-- 		<div id="test2" class="col s12">Test 2</div> -->
		<!-- 		<div id="test4" class="col s12">Test 4</div> -->
	</div>

	<script type="text/javascript" src="../js/buscarUsuario.js"></script>

</body>
</html>