const urlParams = new URLSearchParams(window.location.search);
const dataUsuario = JSON.parse(localStorage.getItem('dataUsuario'))
let codigoUsuario2

$('.modal').modal()

const listarMensajes = () => {
    codigoUsuario2 = parseInt(urlParams.get('codigo_usuario'))

    let json = {
        codigo_usuario: dataUsuario.codigo_usuario,
        codigo_usuario_2: codigoUsuario2
    }

    console.log(json)

    $.ajax({
        url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/listarMensajes',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json',
        data: JSON.stringify(json),
        success: (data) => {
            let sections = ''
            console.log(data)
            data.forEach(v => {
                let pull = ''
                let nickname = v.usuario1
                
                if (v.codigo_usuario_1 == dataUsuario.codigo_usuario) {
                    pull = 'right-align'
                }
                let msg = ''
                if (v.tipo_mensaje == '1') {
                    msg += `<p class='${pull}'>${nickname}</p><p class='${pull}'>${v.mensaje} con el monto de ${v.monto} 
                                <button
                                    class="waves-effect waves-light btn-floating orange">
                                    <i class="material-icons">check</i>
				</button>
                            </p>`
                } else {
                    msg = `<p class='${pull}'>${nickname}</p><p class='${pull}'>${v.mensaje}</p>`
                }

                sections += `<div class="divider"></div>
				<div class="row">
                                    <div class="section col s10 offset-s1">
					${msg}
				</div>
                            </div>`
            })
            $('#divListaMensajes').html(sections)
        }
    })
}

const enviarMensaje = () => {

    let mensaje = $('#txtMensaje').val()

    if (mensaje) {
        codigoUsuario2 = parseInt(urlParams.get('codigo_usuario'))

        let json = {
            codigo_usuario_1: dataUsuario.codigo_usuario,
            codigo_usuario_2: codigoUsuario2,
            mensaje,
            tipo_mensaje: "0"
        }

        $.ajax({
            url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/registrarMensaje',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json',
            data: JSON.stringify(json),
            success: (data) => {
                if (data == '1') {
                    funcionesWebSocket('listarMensajes')
                    $('#txtMensaje').val('')
                    $('#txtMensaje').focus()
                }
            }
        })
    }
}

const registrarCompra = () => {

    let monto = $('#txtMonto').val()

    if (monto) {
        let json = {
            codigo_usuario_1: dataUsuario.codigo_usuario,
            codigo_usuario_2: codigoUsuario2,
            monto,
            tipo_mensaje: "1",
            mensaje: "Se desea realizar una compra"
        }

        $.ajax({
            url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/registrarCompra',
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json',
            data: JSON.stringify(json),
            success: (data) => {
                if (data == '1') {
                    $('.modal').modal('close')
                    funcionesWebSocket('listarMensajes')
                }
            }
        })
    }

}