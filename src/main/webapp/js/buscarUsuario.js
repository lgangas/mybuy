
const dataUsuario = JSON.parse(localStorage.getItem('dataUsuario'))

const welcomeToast = () => {
	M.toast({html: 'Bienvenido '+dataUsuario.nickname})
}

const buscarUsuarios = () => {
	let numero_telefono = $('#txtBuscar').val()
	
	if(numero_telefono != ''){
		let json = {
				numero_telefono
		}
		
		$.ajax({
			url : 'http://localhost:8080/MyBuy/rest/MyBuyRest/buscarTelefono',
			type : 'POST',
			dataType : 'JSON',
			contentType: 'application/json',
			data : JSON.stringify(json),
			success : (data) => {
				let sections = ''
				if(data.length > 0){
					data.forEach(v => {
						sections += '<div class="divider"></div><div class="row">'
						sections += '<div onclick="verPerfil('+ v.codigo_usuario +')" class="section col s10"><h5>'+ v.nickname +'</h5><p>'+ v.numero_telefono +'</p></div><a onclick="listarMensajes('+ v.codigo_usuario +')" class="col s2 center" style="margin-top : 15px"><i class="small material-icons">chat</i></a></div>'						
					})
				} else {
					showToast('Usuario no hallado.')
				}
				$('#divListaUsuario').html(sections)
			}
		})		
	} else {
		showToast('Ingrese un numero de telefono.')
	}
	
}

const verPerfil = (codigo) => {
	location.href = "verPerfil.jsp?codigo_usuario="+codigo
}

const showToast = (message) => {
	M.toast({html: message})
}

const listarMensajes = (codigoUsuario2) => {
	location.href = "chat.jsp?codigo_usuario="+codigoUsuario2
}


