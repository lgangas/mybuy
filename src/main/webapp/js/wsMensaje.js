
var webSocket = new WebSocket(`ws://localhost:8080/MyBuy/UsuarioSesion/${dataUsuario.codigo_usuario}`);
webSocket.onopen = onOpen
webSocket.onmessage = onMessage

function onOpen(message) {
	console.log(message)
}

function onMessage(data) {
	let msg = data.data
	if (msg == 'listarMensajes') {
		listarMensajes()
	}
}

function funcionesWebSocket(accion){
	webSocket.send(accion)
}
