
const registrarUsuario = () => {
	let numero_telefono = $('#txtTelefono').val()
	let contrasenia = $('#txtPassword').val()
	let nickname = $('#txtNickname').val()
	let categoria = $('#txtCategoria').val()
	let numero_cuenta_bancaria = $('#txtNumeroCuenta').val()
	let descripcion = $('#txtDescripcion').val()
	
	let json = {
		numero_telefono,
		contrasenia,
		nickname,
		categoria,
		numero_cuenta_bancaria,
		descripcion
	}
	
	$.ajax({
		url : 'http://localhost:8080/MyBuy/rest/MyBuyRest/registrarUsuario',
		type : 'POST',
		dataType : 'JSON',
		contentType: 'application/json',
		data : JSON.stringify(json),
		success : (data) => {
			if(data === 1){
				location.href = '../vistas/ingresar.jsp'
			} else {
				showToast('Error al registrar.')
			}
		}
	})
}

const showToast = (message) => {
	M.toast({html: message})
}