const dataUsuario = JSON.parse(localStorage.getItem('dataUsuario'))
let codigo_usuario

$('.modal').modal()

const cargarPerfil = () => {
    const urlParams = new URLSearchParams(window.location.search);
    codigo_usuario = parseInt(urlParams.get('codigo_usuario'));

    let json = {
        codigo_usuario
    }

    console.log(json)

    $.ajax({
        url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/datosPerfil',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json',
        data: JSON.stringify(json),
        success: (data) => {
            let sections = ''
            if (data) {
                $('#lblNickaname').html(data.nickname)
                $('#lblTelefono').html(data.numero_telefono)
                $('#lblCategoria').html(data.categoria)
                $('#lblDescripcion').html(data.descripcion)
                $('#lblPuntaje').html(data.promedio_puntaje)
                $('#lblCantidadComentario').html('Comentarios(' + data.cantidad_mensajes + ')')
            } else {
                showToast('Usuario no hallado.')
            }
        }
    })
}

const showToast = (message) => {
    M.toast({html: message})
}

const verComentarios = () => {
    let json = {
        codigo_usuario
    }

    $.ajax({
        url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/listarComentarios',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json',
        data: JSON.stringify(json),
        success: (data) => {
            let sections = ''
            if (data.length > 0) {
                data.forEach(v => {
                    sections += '<div class="divider"></div><div class="section"><label>' + v.nickname + '</label><p>' + v.comentario + '</p></div>'
                })
            } else {
                sections = 'No hay comentarios.'
            }
            $('#divComentarios').html(sections)
        }
    })
}

const comentar = () => {
    let json = {
        codigo_usuario,
        codigo_usuario_2: dataUsuario.codigo_usuario,
        comentario: $('#txtComentario').val()
    }

    $.ajax({
        url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/registrarComentario',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json',
        data: JSON.stringify(json),
        success: (data) => {
            if (data === 1) {
                location.reload()
            } else {
                showToast('No se pudo registrar comentario.')
            }
        }
    })
}

const puntuar = () => {
    let json = {
        codigo_usuario,
        codigo_usuario_2: dataUsuario.codigo_usuario,
        puntaje: parseInt($('#txtPuntaje').val())
    }

    $.ajax({
        url: 'http://localhost:8080/MyBuy/rest/MyBuyRest/registrarPuntaje',
        type: 'POST',
        dataType: 'JSON',
        contentType: 'application/json',
        data: JSON.stringify(json),
        success: (data) => {
            if (data === 1) {
                location.reload()
            } else {
                showToast('No se pudo registrar puntaje.')
            }
        }
    })
}






















