
const ingresarUsuario = () => {
	let numero_telefono = $('#txtTelefono').val()
	let contrasenia = $('#txtPassword').val()
	
	if(numero_telefono != '' && contrasenia != ''){
		let json = {
			numero_telefono,
			contrasenia
		}
		
		$.ajax({
			url : 'http://localhost:8080/MyBuy/rest/MyBuyRest/ingresarUsuario',
			type : 'POST',
			dataType : 'JSON',
			contentType: 'application/json',
			data : JSON.stringify(json),
			success : (data) => {
				if(data.codigo_usuario){
					localStorage.setItem('dataUsuario',JSON.stringify(data))
					location.href = '../vistas/buscarUsuario.jsp'
				} else {
					showToast('Usuario invalido.')
				}
			}
		})
	} else {
		showToast('Ingrese numero de telefono y password.')
	}
}

const showToast = (message) => {
	M.toast({html: message})
}