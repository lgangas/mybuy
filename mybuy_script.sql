create database myBuy;

use myBuy;

create table usuario
(
	codigo_usuario int auto_increment primary key,
    numero_telefonico char(9) not null,
    contrasenia varchar(20) not null,
    nickname varchar(20) not null,
    categoria varchar(30) not null,
    numero_cuenta_bancaria varchar(20) not null,
    descripcion varchar(200) null
);

insert into usuario values (default,'972287773','123456','lgangas','piezas de computadora','4444 6666 2222 8888', 'Encuentranos tambien en facebook.');
insert into usuario values (default,'985745554','123456','cpaz','ropa para mujer','1111 2222 3333 4444', 'Ropa a la moda todo traido de paris y estados unidos.');
select * from usuario;

create table puntaje
(
	codigo_puntaje int auto_increment primary key,
    codigo_usuario_puntuado int not null references usuario,
    codigo_usuario_a_puntuar int not null references usuario,
    puntaje smallint not null
);

insert into puntaje values (default,1,2,8);

create table comentario
(
	codigo_comentario int auto_increment primary key,
    codigo_usuario_emisor int not null references usuario,
    codigo_usuario_receptor int not null references usuario,
    comentario varchar(200) not null
);

create table compra
(
	codigo_compra int auto_increment primary key,
    codigo_usuario_comprador int not null references usuario,
    codigo_usuario_vendedor int not null references usuario,
    monto double not null,
    aceptacion_venta_comprador bit not null,
    aceptacion_venta_vendedor bit not null,
    aceptacion_compra_comprador bit not null,
    aceptacion_compra_vendedor bit not null,
    estado smallint not null -- 0 pendiente | 1 aceptado | 2 anulado
);

create table chat
(
	codigo_chat int auto_increment primary key,
    codigo_usuario_1 int not null references usuario,
    codigo_usuario_2 int not null references usuario
);

create table detalle_chat
(
	codigo_detalle_chat int auto_increment primary key,
    codigo_chat int not null references chat,
    codigo_usuario_emisor int not null references usuario,
    codigo_usuario_receptor int not null references usuario,
    codigo_compra int null references compra,
    mensaje varchar(200) not null,
    tipo_mensaje smallint not null -- 0 mensaje normal | 1 solicitud compra | 2 aceptacion transferencia
);